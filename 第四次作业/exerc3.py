from sets import Set


print("***********注册程序***********\n")
ID = eval(input("只能由数字、字母、下划线组成不超过八位，ID:"))
EMAIL = input("要求符合基本电子邮件格式，email:")
num = eval(input("要求符合基本格式，身份证号:"))
name = input("只能中文且不能超过4位，姓名:")
allowed_chars = Set('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-')
allowed_num = Set('0123456789x')
key1 = 0
key2 = 0
key3 = 0
key4 = 0

while True:
    if 0<len(ID)<8 and Set(ID).issubset(allowed_char):
        print("ID is OK")
        key1 = 1
    else:
        eval(input("只能由数字、字母、下划线组成不超过八位，ID:"))
        continue
    if EMAIL[-1,-7] == "@qq.com":
        print("email is OK")
        key2 = 1
    else:
        EMAIL = eval(input("要求符合基本电子邮件格式，email:"))
        continue
    if len(num) == 18 and Set(num[-1]).issubset(allowed_num):
        print("身份证 is OK")
        key3 = 1
    else:
        num = eval(input("要求符合基本格式，身份证号:"))
        continue
    if 0<len(name)<4:
        for ch in name.decode('utf-8'):
            if u'\u4e00' <= ch <= u'\u9fff':
                print("name is OK")
                key4 = 1
    else:
        name = input("只能中文且不能超过4位，姓名:")
        continue
    if key1 == 1 and key2 == 1 and key3 == 1 and key4 == 1:
        break
print(ID,",",EMAIL,",",num,",",name)
