
def enCode(message):
    #要加解密的字符串message

    #加解密密钥
    key=13

    #程序是加密还是解密
    mode='encrypt' #设置为encrypt或decrypt

    #可能被加解密的符号
    SYMBOLS='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 !?.'

    #存储消息的加解密形式
    translated=''

    for symbol in message:
        #注意：只能加解密SYMBOLS字符串中的符号
        if symbol in SYMBOLS:
            symbolIndex=SYMBOLS.find(symbol)

            #执行加解密
            if mode =='encrypt':
                translatedIndex=symbolIndex+key
            if mode =='decrypt':
                translatedIndex=symbolIndex-key

            #如果需要，执行回环
            if translatedIndex>=len(SYMBOLS):
                translatedIndex=translatedIndex-len(SYMBOLS)
            elif translatedIndex <= 0:
                translatedIndex=translatedIndex+len(SYMBOLS)

            translated=translated+SYMBOLS[translatedIndex]

        else:
            #添加未加解密的字符
            translated=translated+symbol

    #输出translated字符串
    return translated

def sub(string,p,c):
    new = []
    for s in string:
        new.append(s)
    new[p] = c
    return ''.join(new)
    
def reverse(str1):
    for i in range(len(str1)):
        if str1[i].islower():
            str1 = sub(str1,i,str1[i].upper())
        else:
            str1 = sub(str1,i,str1[i].lower())
    return str1
    

def letter(str1):
    j = len(str1)
    for i in range(j):
        if not str1[i].isalpha():
            str1 = str1.replace(str1[i],' ',1)
    str1 = str1.replace(" ","")
    return str1

def main():
    while True:
        str1 = input("输入一串字符:\n")
        if 0<len(str1)<=255:
            break
        else:
            print("输入不符合要求，重新输入")
    str2 = letter(str1)
    print("剔除非英文字符:",str2)
    str3 = reverse(str2)
    print("大小写反转:",str3)
    str4 = enCode(str3)
    print("加密后:",str4)

main()
