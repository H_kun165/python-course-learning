import jieba

text = open("C:\\Users\\Administrator\\Desktop\\三体.txt","r",encoding="utf-8").read()
content = jieba.lcut(text)
stop = {"我们","他们","一个","自己","没有","这个","已经","你们","可以","知道","什么","大家","现在","计划","时候","星人"}
counts = {}
for word in content:
    if word not in stop:
        if len(word) == 1:
            continue
        else:
            counts[word] = counts.get(word,0)+1
items = list(counts.items())
items.sort(key=lambda x:x[1],reverse=True)
for i in range(6):
    word,count = items[i]
    print("{0:<10}{1:<5}".format(word,count))
