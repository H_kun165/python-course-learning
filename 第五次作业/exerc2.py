height,weight = eval(input("输入身高和体重 单位为米和公斤【逗号隔开】"))
bmi = weight/pow(height,2)
print("BMI为:{:.2f}".format(bmi))

wto = ""
if bmi<18.5:
    wto = "偏瘦"
elif bmi<25:
    wto = "正常"
elif bmi<30:
    wto = "偏胖"
else:
    wto = "肥胖"

print("BMI指标为:{}".format(wto))
